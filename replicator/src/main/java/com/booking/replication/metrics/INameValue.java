package com.booking.replication.metrics;

import java.math.BigInteger;

/**
 * Created by mdutikov on 5/31/2016.
 */
public interface INameValue {
    String getName();

    BigInteger getValue();
}
